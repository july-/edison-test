import { Component, OnInit } from '@angular/core';
import {DataService} from './../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private dataService:DataService) { }

  breaches = [];
	
	showSpinner = true;
	
	ngOnInit() {
		this.getBreaches();
	}
	
	getBreaches() {
   this.dataService.getBreaches().subscribe(
      data => { this.breaches = data; this.showSpinner = false;},
      err => {console.error(err); this.showSpinner = false;}
    );
  }
}
