import { Component, OnInit } from '@angular/core';
import {DataService} from './../data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

	companies = [];
	companyField = '';
	nameField = '';
	resultNameField = '';
	showSpinner = true;
	searchResult = [];
	searchStatus = '';
	
  constructor(private dataService:DataService) { }

  ngOnInit() {
		this.getBreaches();
  }
	
	getBreaches() {
   this.dataService.getBreaches().subscribe(
      data => { this.companies = data; this.showSpinner = false;},
      err => {console.error(err); this.showSpinner = false;}
    );
  }
	
	submitSearch(form){
		this.showSpinner = true;
		this.searchStatus = "started";
		var userName = form.value.username,
		companyName = form.value.company;
    this.dataService.getBreachesForName(userName).subscribe(
      data => { 
				this.searchResult = this.filterResult(data, companyName); 
				this.showSpinner = false;
				this.resultNameField = userName;
				this.searchStatus = "finished";
			},
      err => {console.error(err); this.showSpinner = false;}
    );
		form.reset();
  }
	
	filterResult(data, company) {
		if (company == '' || company == null) {
			return data;
		}
		
		return data.filter(function (el) {
			return el.Name == company;
		});
	}
	

}
