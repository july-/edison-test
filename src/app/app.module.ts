import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';


import { AppComponent } from './app.component';
import {DataService} from './data.service';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { SpinnerComponent } from './spinner/spinner.component';


const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'search', component: SearchComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    SpinnerComponent
  ],
  imports: [
		RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } 
    ),
    BrowserModule,
		HttpClientModule,
		FormsModule,
		NgSelectModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
