import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
 
@Injectable()
export class DataService {

  apiRoot:string = 'https://haveibeenpwned.com/api/v2';
  constructor(private http:HttpClient) {}
 
    // Uses http.get() to load data from a single API endpoint
  getBreaches() {
      return this.http.get<any[]>(this.apiRoot + '/breachedaccount/test@gmail.com');
  }
	
	getBreachesForName(username) {
      return this.http.get<any[]>(this.apiRoot + '/breachedaccount/' + username);
  }

}
